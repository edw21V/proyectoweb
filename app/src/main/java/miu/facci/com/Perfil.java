package miu.facci.com;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import miu.facci.com.client.ClientRetrofit;
import miu.facci.com.client.RetroClient;
import miu.facci.com.entity.Follower;
import miu.facci.com.listView.List_adapter;
import miu.facci.com.listView.RowItem;

import java.util.ArrayList;
import java.util.List;

import miu.facci.com.R;

import miu.facci.com.client.GlobalData;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Perfil extends AppCompatActivity {

    private List<Follower> followerList = new ArrayList<>();
    private ListView listView;
    private ProgressBar progressBar;
    List<RowItem> rowItems;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_perfil);
        progressBar = (ProgressBar) findViewById(R.id.progressBar2);
        progressBar.setVisibility(View.VISIBLE);
        rowItems = new ArrayList<>();

        GlobalData globalData = (GlobalData)Perfil.this.getApplication();//Instancia de la clase GlobalData
        TextView repos = (TextView) findViewById(R.id.textView_nrepo);
        repos.setText(globalData.getUser().getPublic_repos());
        TextView followers = (TextView) findViewById(R.id.textView_nfollow);
        followers.setText(globalData.getUser().getFollowers());//Envia los nombres de los seguidores
        String follower = globalData.getUser().getFollowers();
        if(!follower.equals("0")) getFollowers();
        else progressBar.setVisibility(View.GONE);
    }

    public void getFollowers(){
        ClientRetrofit clientRetrofit = RetroClient.getClientRetrofit();//inicializa cliente retrofit
        GlobalData globalData= (GlobalData)Perfil.this.getApplication();//instancia de la clase Global
        String username= globalData.getLogin();//Guarda nombre de usuario
        Call<List<Follower>> getFollowersCall = clientRetrofit.getGITFollowers(username);//Llamada a retrofit para obtener lista de los seguidores
        getFollowersCall.enqueue(new Callback<List<Follower>>() {
            @Override
            public void onResponse(Call<List<Follower>> call, Response<List<Follower>> response) {
                followerList = response.body();//Obtenemos la lista de los seguidores de la entidad Followers
                progressBar.setVisibility(View.GONE);
                printList();
            }

            @Override
            public void onFailure(Call<List<Follower>> call, Throwable t) {
                call.cancel();//cancelar solicitud
                progressBar.setVisibility(View.GONE);//de baja
                Toast.makeText(Perfil.this, "Error en obtener a seguidores", Toast.LENGTH_LONG).show();
            }
        });

    }

    public void printList(){
        for (int i = 0; i < followerList.size(); i++){
            RowItem item = new RowItem(R.drawable.boy,followerList.get(i).getLogin());//Lista de los seguidores foto y nombre
            rowItems.add(item);
        }
        listView = (ListView) findViewById(R.id.list);
        List_adapter adapter = new List_adapter(this,R.layout.activity_entry,rowItems);
        listView.setAdapter(adapter);
    }

}
